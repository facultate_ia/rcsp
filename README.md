# RCSP - Redactare si Comunicare Stiintifica si Profesionala

## Recomand instalarea extensiei de Latex pt Visual Studio Code, ca sa nu trebuiasca instalate toate pachetele pentru TexStudio (care oricum e cam naspa :)) )

- [Aici](./Cursuri) sunt cursurile si modul de evaluare
- [Aici](./Laboratoare) sunt laboratoarele rezolvate care trebuie predate la un moment dat
- [Aici](./Referat) e referatul/eseul de evaluarea finala